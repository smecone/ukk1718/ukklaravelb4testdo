<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name');
            $table->string('username');
            $table->string('password');

            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('description');
            
            $table->timestamps();
        });

        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name');
            $table->string('description');
            
            //Relations
            $table->integer('category_id')->unsigned()->nullable()->index();
            $table->foreign('category_id')->references('id')->on('categories');

            $table->timestamps();
        });

        Schema::create('lending_books', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('date_borrow');
            $table->dateTime('date_return');
            $table->integer('status')->default(0);

            // Relations
            $table->integer('book_id')->unsigned()->nullable()->index();
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->integer('admin_id')->unsigned()->nullable()->index();
            $table->foreign('book_id')->references('')->on('');
            $table->foreign('user_id')->references('')->on('');
            $table->foreign('admin_id')->references('')->on('');                        
            
            $table->timestamps();
        });

        Schema::create('return_books', function (Blueprint $table) {
            $table->increments('id');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
